import * as mongoose from "mongoose"; 
//import { _HOST, _PORT } from "./Connection";
import { tmpdir } from "os";


//Schema
const CounterSchema =new mongoose.Schema({
    i: {type: Number,  default: 1}, //required: true,
    logLastUpdate: {type: Date, default: Date.now},
    working: Boolean,
    name: String
})

//export abstraction counter
let counter: mongoose.Model<any>=null;

//establish a connection
mongoose.connect("mongodb://127.0.0.1/db")//`mongodb://${_HOST}:${_PORT}/db`
    .then(connection=>{
        counter=connection.model('counter',CounterSchema)//mapping the schema "Counter" and export on local var "counter"
        console.log(`schema ${CounterSchema} mapped on local var ${counter}`)
    })
    .catch(e => console.log("connection refused... ",e))


    //crete schema 
function fillCounter(iterator: number, working: boolean, name: string){
    counter.create({ i: iterator, working: working, name:name })
        .then(()=>{
            console.log(`added on counter model iterator: ${iterator} and boolean working ${working}`)
        })
        .catch(e => {console.log("there is a issue for creating a new counter ",e)})

       
}




//increment counter
function increment(nome: string){
    var n: number

    counter.findOne({nome, working:true})
        .exec(function (error, cnt){
            n=cnt.i;
        })

      
    counter.updateOne({nome, working:true}, {i:n})
}




fillCounter(1,true, "mario");

//set this between interval
//set this between interval

//while(true)
//setTimeout(increment("mario"), 5000) //o mettere setTimwout(increment("mario") {operazione di incremento vera e proria...})


//increment("mario");

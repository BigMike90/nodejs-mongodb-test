"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
//Schema
var CounterSchema = new mongoose.Schema({
    i: { type: Number, default: 1 },
    logLastUpdate: { type: Date, default: Date.now },
    working: Boolean,
    name: String
});
//export abstraction counter
var counter = null;
//establish a connection
mongoose.connect("mongodb://127.0.0.1/db") //`mongodb://${_HOST}:${_PORT}/db`
    .then(function (connection) {
    counter = connection.model('counter', CounterSchema); //mapping the schema "Counter" and export on local var "counter"
    console.log("schema " + CounterSchema + " mapped on local var " + counter);
})
    .catch(function (e) { return console.log("connection refused... ", e); });
//crete schema 
function fillCounter(iterator, working, name) {
    counter.create({ i: iterator, working: working, name: name })
        .then(function () {
        console.log("added on counter model iterator: " + iterator + " and boolean working " + working);
    })
        .catch(function (e) { console.log("there is a issue for creating a new counter ", e); });
}
//increment counter
function increment(nome) {
    var n;
    counter.findOne({ nome: nome, working: true })
        .exec(function (error, cnt) {
        n = cnt.i;
    });
    counter.updateOne({ nome: nome, working: true }, { i: n });
}
fillCounter(1, true, "mario");
//set this between interval

//while(true)
//setTimeout(increment("mario"), 5000) //o mettere setTimwout(increment("mario") {operazione di incremento vera e proria...})


//increment("mario");


